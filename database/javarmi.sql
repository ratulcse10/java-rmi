-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 09, 2013 at 08:24 PM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `javarmi`
--

-- --------------------------------------------------------

--
-- Table structure for table `follow_record`
--

CREATE TABLE IF NOT EXISTS `follow_record` (
  `follow` varchar(100) NOT NULL,
  `follower` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `follow_record`
--

INSERT INTO `follow_record` (`follow`, `follower`) VALUES
('test@test.com', 'john@gmail.com'),
('john@gmail.com', 'test@test.com');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE IF NOT EXISTS `message` (
  `userOne` varchar(100) NOT NULL,
  `userTwo` varchar(100) NOT NULL,
  `message` varchar(10000) NOT NULL,
  `dateTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`userOne`, `userTwo`, `message`, `dateTime`) VALUES
('john@gmail.com', 'test@test.com', 'Hello How are You ???', '2013-05-10 00:03:56'),
('test@test.com', 'john@gmail.com', 'fine, you ?', '2013-05-10 00:07:12'),
('john@gmail.com', 'test@test.com', 'i am also fine :)', '2013-05-10 00:07:26'),
('test@test.com', 'john@gmail.com', 'will you go with me there ?', '2013-05-10 00:07:47'),
('john@gmail.com', 'test@test.com', 'ok :) when ?', '2013-05-10 00:08:15'),
('test@test.com', 'john@gmail.com', 'tonight at 10 PM', '2013-05-10 00:08:31'),
('john@gmail.com', 'test@test.com', 'ok :)', '2013-05-10 00:16:57'),
('test@test.com', 'john@gmail.com', ':)', '2013-05-10 00:17:32'),
('john@gmail.com', 'test@test.com', ':)', '2013-05-10 00:17:50'),
('test@test.com', 'john@gmail.com', ':) :)', '2013-05-10 00:19:56'),
('test@test.com', 'john@gmail.com', ':D :D', '2013-05-10 00:20:24'),
('test@test.com', 'john@gmail.com', ':P :P', '2013-05-10 00:22:37');

-- --------------------------------------------------------

--
-- Table structure for table `tweet`
--

CREATE TABLE IF NOT EXISTS `tweet` (
  `email` varchar(200) NOT NULL,
  `tweet` varchar(500) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tweet`
--

INSERT INTO `tweet` (`email`, `tweet`, `date`) VALUES
('john@gmail.com', 'Nice Day !!', '2013-05-04'),
('john@gmail.com', '#What are you doing now ??', '2013-05-04'),
('ornob@gmail.com', 'Bangladesh is a Nice Country !!!', '2013-05-04'),
('ornob@gmail.com', '#Bangladesh is a really Nice Country !!!', '2013-05-04'),
('ornob@gmail.com', '#Cricket !!!!', '2013-05-04'),
('john@gmail.com', 'How Nice !!', '2013-05-05'),
('john@gmail.com', '#How Nice the Day!!', '2013-05-05'),
('a', 'Happy Day !!', '2013-05-08'),
('a', 'Happy Day !!', '2013-05-08');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `name` varchar(200) NOT NULL,
  `email` varchar(50) NOT NULL,
  `pass` varchar(50) NOT NULL,
  PRIMARY KEY (`email`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`name`, `email`, `pass`) VALUES
('Jamal', 'jamal@gmail.com', '1'),
('John', 'john@gmail.com', '1'),
('Kamal', 'kamal@gmail.com', '1'),
('Ornob', 'ornob@gmail.com', '1'),
('Rahim', 'rahim@gmail.com', '1'),
('Test', 'test@test.com', '1');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
