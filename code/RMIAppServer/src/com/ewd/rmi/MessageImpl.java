package com.ewd.rmi;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;




/**
 * This Class Implement all methods for communication with server and client. 
 */

public class MessageImpl extends UnicastRemoteObject implements Message {

    // JDBC driver name and database URL
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost/javarmi";
    //  Database credentials
    static final String USER = "root";
    static final String PASS = "";
    Connection conn = null;
    Statement stmt = null;
    Statement stmt2=null;

    public MessageImpl() throws RemoteException {
    }

  
/**
   * This method check the user login validity
   * @return String Array with username and email.
   * @exception IOException On input error.
*/
    public String[] login(String email, String pass) throws RemoteException {



        try {
            //STEP 2: Register JDBC driver
            Class.forName("com.mysql.jdbc.Driver");
            //STEP 3: Open a connection
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            //STEP 4: Execute a query
            stmt = conn.createStatement();
            String sql = "SELECT * FROM user";
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                if (email.equals(rs.getString("email")) && pass.equals(rs.getString("pass"))) {
                    String[] loginReturn = {rs.getString("name"), rs.getString("email")};
                    return loginReturn;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(Map.Entry.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Map.Entry.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }


/**
   * This method inputs the information for registration of a new client.
   * @return a flag int.
   * @exception IOException On input error.
*/
    public int register(String name, String email, String pass) throws RemoteException {
        try {
            //STEP 2: Register JDBC driver
            Class.forName("com.mysql.jdbc.Driver");
            //STEP 3: Open a connection
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            //STEP 4: Execute a query
            stmt = conn.createStatement();
            String sql = "INSERT into user VALUES ('" + name + "','" + email + "','" + pass + "')";
            stmt.executeUpdate(sql);
            JOptionPane.showMessageDialog(null, "Registration Successfull !!!", "Successfull", JOptionPane.INFORMATION_MESSAGE);
            return 1;
        } catch (SQLException ex) {
            Logger.getLogger(Map.Entry.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Registration Failed !!! Try Again", "Error !!", JOptionPane.ERROR_MESSAGE);
            return 0;
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Map.Entry.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Registration Failed !!! Try Again", "Error !!", JOptionPane.ERROR_MESSAGE);
            return 0;
        }
    }

/**
   * This method save the tweets by user in database.
   * @exception IOException On input error.
*/
    public void tweet(String tweetTextReceive, String emailID) throws RemoteException {
        try {
            //STEP 2: Register JDBC driver
            Class.forName("com.mysql.jdbc.Driver");
            //STEP 3: Open a connection
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            //STEP 4: Execute a query
            stmt = conn.createStatement();

            Calendar currentDate = Calendar.getInstance(); //Get the current date
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd"); //format it as per your requirement
            String dateNow = formatter.format(currentDate.getTime());

            String sql = "INSERT into tweet VALUES ('" + emailID + "','" + tweetTextReceive + "','" + dateNow + "')";
            stmt.executeUpdate(sql);
            JOptionPane.showMessageDialog(null, "Tweet has been Saved Successfull !!!", "Successfull", JOptionPane.INFORMATION_MESSAGE);

        } catch (SQLException ex) {
            Logger.getLogger(Map.Entry.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Map.Entry.class.getName()).log(Level.SEVERE, null, ex);

        }
    }

/**
   * This method return the client total information of the users to follow.
   * @return ArrayList of All user information.
   * @exception IOException On input error.
*/
    public ArrayList<String> getUserToFollow(String myEmail) throws RemoteException  {
        try {
            //STEP 2: Register JDBC driver
            Class.forName("com.mysql.jdbc.Driver");
            //STEP 3: Open a connection
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            //STEP 4: Execute a query
            stmt = conn.createStatement();
            stmt2 = conn.createStatement();
            
            String[] myFollow = new String[500] ;
            int f=0;
            
            String sqlCheck = "SELECT * FROM follow_record where follower='"+myEmail+"'";
            ResultSet rs1 = stmt.executeQuery(sqlCheck);
            while (rs1.next()) {
                String email = new String();
                email = rs1.getString("follow");
                myFollow[f]=email;
                f++;
            }
            
            
            
            
            String sql = "SELECT * FROM user order by name ASC";
            ResultSet rs = stmt2.executeQuery(sql);
            ArrayList<String> userDetailsReturn = new ArrayList<String>();
            int confirmFollow=1;
            while (rs.next()) {
                String name = new String();
                String email = new String();

                name = rs.getString("name");
                email = rs.getString("email");
                for (int l=0;l<myFollow.length;l++){
                   if(email.equals(myFollow[l])){
                       confirmFollow=0;
                   } 
                }
                if(confirmFollow==1){
                userDetailsReturn.add(name);
                userDetailsReturn.add(email);   
                }
                else{
                    confirmFollow=1;
                }
            }
            return userDetailsReturn;
        } catch (SQLException ex) {
            Logger.getLogger(Map.Entry.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Map.Entry.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

/**
   * This method set the user as client following.
   * @exception IOException On input error.
*/
    public void setFollow(String myEmail, String followEmail) throws RemoteException {
        try {
            //STEP 2: Register JDBC driver
            Class.forName("com.mysql.jdbc.Driver");
            //STEP 3: Open a connection
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            //STEP 4: Execute a query
            stmt = conn.createStatement();
           
            String sql = "INSERT into follow_record VALUES ('" + followEmail + "','" + myEmail + "')";
            stmt.executeUpdate(sql);
            String news = "You have Successfully followed ".concat(followEmail);
            JOptionPane.showMessageDialog(null, news, "Successfull", JOptionPane.INFORMATION_MESSAGE);
            }
            catch (SQLException ex) {
            Logger.getLogger(Map.Entry.class.getName()).log(Level.SEVERE, null, ex);
             } catch (ClassNotFoundException ex) {
            Logger.getLogger(Map.Entry.class.getName()).log(Level.SEVERE, null, ex);

        }

        } 

/**
   * This method return the all data of users who follow the client.
   * @return String Arraylist.
   * @exception IOException On input error.
*/
    public ArrayList<String> getFollower(String myMail) throws RemoteException {
     ArrayList<String> myFollowers = new ArrayList<String>();
     try {
            //STEP 2: Register JDBC driver
            Class.forName("com.mysql.jdbc.Driver");
            //STEP 3: Open a connection
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            //STEP 4: Execute a query
            stmt = conn.createStatement();
            stmt2 = conn.createStatement();
            
            String[] myFollow = new String[500] ;
            int f=0;
            
            String sqlCheck = "SELECT * FROM follow_record where follow='"+myMail+"'";
            ResultSet rs1 = stmt.executeQuery(sqlCheck);
            while (rs1.next()) {
                String email = new String();
                email = rs1.getString("follower");
                myFollow[f]=email;
                f++;
            }
            
            
            
            
            String sql = "SELECT * FROM user order by name ASC";
            ResultSet rs = stmt2.executeQuery(sql);
            while (rs.next()) {
                String name = new String();
                String email = new String();

                name = rs.getString("name");
                email = rs.getString("email");
                
                for (int l=0;l<myFollow.length;l++){
                   if(email.equals(myFollow[l])){
                     myFollowers.add(name);
                       
                   } 
                }
            }
            return  myFollowers;
        } catch (SQLException ex) {
            Logger.getLogger(Map.Entry.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Map.Entry.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
     
     
     
    }

/**
   * This method give the client about all information of the following user of him.
   * @return String Arraylist.
   * @exception IOException On input error.
*/
    public ArrayList<String> getFollowing(String myMail) throws RemoteException {
     ArrayList<String> myFollowings = new ArrayList<String>();
     try {
            //STEP 2: Register JDBC driver
            Class.forName("com.mysql.jdbc.Driver");
            //STEP 3: Open a connection
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            //STEP 4: Execute a query
            stmt = conn.createStatement();
            stmt2 = conn.createStatement();
            
            
            String[] myFollowing = new String[1000];
            int following=0;
            
            String sqlCheck = "SELECT * FROM follow_record where follower='"+myMail+"'";
            ResultSet rs1 = stmt.executeQuery(sqlCheck);
            while (rs1.next()) {
                String email = new String();
                email = rs1.getString("follow");
                myFollowing[following]=email;
                following++;
            }
            
            
            
            
            
            String sql = "SELECT * FROM user order by name ASC";
            ResultSet rs = stmt2.executeQuery(sql);
            while (rs.next()) {
                String name = new String();
                String email = new String();

                name = rs.getString("name");
                email = rs.getString("email");
                
                for (int l=0;l<myFollowing.length;l++){
                   if(email.equals(myFollowing[l])){
                     myFollowings.add(name);
                     myFollowings.add(email);
                   } 
                }
            }

            return  myFollowings;
        } catch (SQLException ex) {
            Logger.getLogger(Map.Entry.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Map.Entry.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

/**
   * This method return the user all tweets of the users followed by him.
   * @return String Arraylist.
   * @exception IOException On input error.
*/
    public ArrayList<String> getTweets(String email) throws RemoteException {
        ArrayList<String> tweets = new ArrayList<String>();
     try {
            //STEP 2: Register JDBC driver
            Class.forName("com.mysql.jdbc.Driver");
            //STEP 3: Open a connection
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            //STEP 4: Execute a query
            stmt = conn.createStatement();
            stmt2 = conn.createStatement();
            
            
            String sqlCheck = "SELECT * FROM tweet where email='"+email+"' order by date DESC";
            ResultSet rs1 = stmt.executeQuery(sqlCheck);
            while (rs1.next()) {
                String tweet = new String();
                tweet = rs1.getString("tweet");
                tweets.add(rs1.getString("date"));
                tweets.add(tweet);
            }
            return  tweets;
        } catch (SQLException ex) {
            Logger.getLogger(Map.Entry.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Map.Entry.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
      }

/**
   * This method return the client a search result of any tweet word.
   * @return String Arraylist.
   * @exception IOException On input error.
*/
    public ArrayList<String> getSearchTweets(String search) throws RemoteException {
         ArrayList<String> Searchtweets = new ArrayList<String>();
     try {
            //STEP 2: Register JDBC driver
            Class.forName("com.mysql.jdbc.Driver");
            //STEP 3: Open a connection
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            //STEP 4: Execute a query
            stmt = conn.createStatement();
            stmt2 = conn.createStatement();
            
            
            String sqlCheck = "SELECT * FROM tweet where tweet LIKE '#"+search+"%' order by date DESC";
            ResultSet rs1 = stmt.executeQuery(sqlCheck);
            while (rs1.next()) {
                String tweet = new String();
                tweet = rs1.getString("tweet");
                Searchtweets.add(rs1.getString("email"));
                Searchtweets.add(rs1.getString("date"));
                Searchtweets.add(tweet);
            }
            return  Searchtweets;
        } catch (SQLException ex) {
            Logger.getLogger(Map.Entry.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Map.Entry.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

/**
   * This method unfollow any user from following list of client.
   * @exception IOException On input error.
*/
    public void unFollow(String myEmail, String unFollowEmail) throws RemoteException {
            try {
            //STEP 2: Register JDBC driver
            Class.forName("com.mysql.jdbc.Driver");
            //STEP 3: Open a connection
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            //STEP 4: Execute a query
            stmt = conn.createStatement();
           
            String sql = "DELETE FROM follow_record where follow='"+unFollowEmail+"' and follower='"+myEmail+"' ";
            stmt.executeUpdate(sql);
            String news = "You have Successfully unfollowed ".concat(unFollowEmail);
            JOptionPane.showMessageDialog(null, news, "Successfull", JOptionPane.INFORMATION_MESSAGE);
            }
            catch (SQLException ex) {
            Logger.getLogger(Map.Entry.class.getName()).log(Level.SEVERE, null, ex);
             } catch (ClassNotFoundException ex) {
            Logger.getLogger(Map.Entry.class.getName()).log(Level.SEVERE, null, ex);

        }
         
    }

/**
   * This method reurn the username searched by word.
   * @return String Arraylist.
   * @exception IOException On input error.
*/
    public ArrayList<String> getSearchUser(String searchUser,String myEmail) throws RemoteException {
        try {
            //STEP 2: Register JDBC driver
            Class.forName("com.mysql.jdbc.Driver");
            //STEP 3: Open a connection
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            //STEP 4: Execute a query
            stmt = conn.createStatement();
            stmt2 = conn.createStatement();
            
            String[] myFollow = new String[500] ;
            int f=0;
            
            String sqlCheck = "SELECT * FROM follow_record where follower='"+myEmail+"'";
            ResultSet rs1 = stmt.executeQuery(sqlCheck);
            while (rs1.next()) {
                String email = new String();
                email = rs1.getString("follow");
                myFollow[f]=email;
                f++;
            }
            
            
            
            
            String sql = "SELECT * FROM user where name LIKE '%"+searchUser+"%' order by name ASC ";
            ResultSet rs = stmt2.executeQuery(sql);
            ArrayList<String> userDetailsReturn = new ArrayList<String>();
            int confirmFollow=1;
            while (rs.next()) {
                String name = new String();
                String email = new String();

                name = rs.getString("name");
                email = rs.getString("email");
                for (int l=0;l<myFollow.length;l++){
                   if(email.equals(myFollow[l])){
                       confirmFollow=0;
                   } 
                }
                if(confirmFollow==1){
                userDetailsReturn.add(name);
                userDetailsReturn.add(email);   
                }
                else{
                    confirmFollow=1;
                }
            }
            return userDetailsReturn;
        } catch (SQLException ex) {
            Logger.getLogger(Map.Entry.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Map.Entry.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
     }

    @Override
    public void saveMessage(String myEmail, String email,String message) throws RemoteException {
        try {
            //STEP 2: Register JDBC driver
            Class.forName("com.mysql.jdbc.Driver");
            //STEP 3: Open a connection
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            //STEP 4: Execute a query
            stmt = conn.createStatement();

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            //get current date time with Date()
            java.util.Date date = new java.util.Date();
            String dateNow = dateFormat.format(date);

            String sql = "INSERT into message VALUES ('" + myEmail + "','" + email + "','"+message+"', '" + dateNow + "')";
            stmt.executeUpdate(sql);
            JOptionPane.showMessageDialog(null, "Message Sent !!!", "Successfull", JOptionPane.INFORMATION_MESSAGE);

        } catch (SQLException ex) {
            Logger.getLogger(Map.Entry.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Error !!!", "Error!!", JOptionPane.ERROR_MESSAGE);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Map.Entry.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Error !!!", "Error!!", JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public ArrayList<String> getMessage(String myEmail, String email) throws RemoteException {
        try {
             ArrayList<String> messageDetailsReturn = new ArrayList<String>();
            //STEP 2: Register JDBC driver
            Class.forName("com.mysql.jdbc.Driver");
            //STEP 3: Open a connection
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            //STEP 4: Execute a query
            stmt = conn.createStatement();

            String sql = "SELECT * FROM message where (userOne='"+myEmail+"' and userTwo='"+email+"') or (userOne='"+email+"' and userTwo='"+myEmail+"') order by dateTime ASC ";
            ResultSet rs = stmt2.executeQuery(sql);
 
            while (rs.next()) {
             messageDetailsReturn.add(rs.getString("userOne"));
             messageDetailsReturn.add(rs.getString("dateTime"));
             messageDetailsReturn.add(rs.getString("message"));
            }
            return messageDetailsReturn;
        } catch (SQLException ex) {
            Logger.getLogger(Map.Entry.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Map.Entry.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
   }
