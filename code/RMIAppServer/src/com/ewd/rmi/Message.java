package com.ewd.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

public interface Message extends Remote {

     public String[] login(String email,String pass) throws RemoteException;
     public int register (String name, String email, String pass) throws RemoteException;
     public void tweet(String tweetTextReceive, String emailID) throws RemoteException;
     public ArrayList<String> getUserToFollow(String myEmail) throws RemoteException ;
     public void setFollow(String myEmail,String followEmail) throws RemoteException;
     public ArrayList<String> getFollower(String myMail) throws RemoteException;
     public ArrayList<String> getFollowing(String myMail) throws RemoteException;
     public ArrayList<String> getTweets(String email) throws RemoteException;
     public ArrayList<String> getSearchTweets(String search) throws RemoteException;
     public void unFollow(String myEmail,String unFollowEmail) throws RemoteException;
     public ArrayList<String> getSearchUser(String searchUser,String myEmail) throws RemoteException ;
     public void saveMessage(String myEmail,String email,String message) throws RemoteException;
     public ArrayList<String> getMessage(String myEmail,String email) throws RemoteException;
}